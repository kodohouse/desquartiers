<?php

namespace Sspoon\Backend\Components;


use Request;
use Sspoon\Backend\Models\Contact;
use Sspoon\Backend\Models\Eggs;
use Sspoon\Backend\Models\Professions;
use Sspoon\Backend\Models\Propos;
use Sspoon\Backend\Models\Realisation;


class Descriptions extends \Cms\Classes\ComponentBase
{
    public function componentDetails()
    {
        return [
            'name' => 'Infos',
            'description' => 'Display infos.'
        ];
    }


    public function onRun()
    {
        $this->page['contact'] = Contact::instance();
        $this->page['profession'] = Professions::find(1);
        $this->page['propos'] = Propos::instance();
        $this->page['realisations'] = Realisation::orderBy('sort_order', 'DESC')->get();
        $this->page['rFirstSale'] = Realisation::where('is_on_sale', '=',1)->orderBy('sort_order', 'DESC')->first();
        $this->page['rFirstNotSale'] = Realisation::where('is_on_sale', '=',0)->orWhereNull('is_on_sale')->orderBy('sort_order', 'DESC')->first();

        $this->page['eggs'] = Eggs::all();

    }


}
