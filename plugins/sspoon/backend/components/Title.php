<?php

namespace Sspoon\Backend\Components;

use Request;
use Sspoon\Backend\Models\Category;


class Title extends \Cms\Classes\ComponentBase
{
    public function componentDetails()
    {
        return [
            'name' => 'Title',
            'description' => 'Title'
        ];
    }

    public function onRun()
    {

        if ($this->page->id == 'realisation' && $this->param('slug')) {
            $v = \Sspoon\Backend\Models\Realisation::where('slug', '=', $this->param('slug'))->first();

            $this->page->title = $v->title;
            $this->page->meta_description = $v->description;
            $this->page['realisation']=$v;
        }
        else
            if ($this->page->id == 'egg' && $this->param('slug')) {
                $v = \Sspoon\Backend\Models\Eggs::where('slug', '=', $this->param('slug'))->first();

                $this->page->title = $v->title;
                $this->page->meta_description = $v->description;
                $this->page['egg']=$v;
            }


    }


}
