<?php namespace Sspoon\Backend\Controllers;

use Backend\Classes\Controller;
use BackendMenu;

class Professions extends Controller
{
    public $implement = [        'Backend\Behaviors\FormController'    ];
    
    public $formConfig = 'config_form.yaml';

    public function __construct()
    {
        parent::__construct();
        BackendMenu::setContext('Sspoon.Backend', 'main-menu-metiers');
    }
}
