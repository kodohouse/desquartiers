<?php namespace Sspoon\Backend\Controllers;

use Backend\Classes\Controller;
use BackendMenu;
use Input;
use October\Rain\Support\Facades\Flash;
use RainLab\Translate\Models\Locale;
use Sspoon\Backend\Models\Contact as ContactModel;

class Contact extends Controller
{
    public $implement = [];

    public function __construct()
    {
        parent::__construct();

        BackendMenu::setContext('Sspoon.Backend', 'main-menu-contact');
    }

    public function index()
    {
        $s = ContactModel::instance();
        $this->vars['data'] = $s;
    }

    public function onUpdate()
    {
        $input = Input::get('data');
        $s = ContactModel::instance();

        $s->fill($input)->save();
        Flash::success('Data successfully saved!');
        return \Redirect::to('/backend/sspoon/backend/contact');
    }


}
