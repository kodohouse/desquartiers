<?php namespace Sspoon\Backend\Controllers;

use Backend\Classes\Controller;
use BackendMenu;

class Realisations extends Controller
{
    public $implement = [        'Backend\Behaviors\ListController',
        'Backend\Behaviors\FormController',
        'Backend\Behaviors\ReorderController'    ];
    
    public $listConfig = 'config_list.yaml';
    public $formConfig = 'config_form.yaml';
    public $reorderConfig = 'config_reorder.yaml';

    public function __construct()
    {
        parent::__construct();
        BackendMenu::setContext('Sspoon.Backend', 'main-menu-realisations');
    }

    public function reorderExtendQuery($query)
    {

        $query->orderBy('sort_order', 'desc');
    }


}
