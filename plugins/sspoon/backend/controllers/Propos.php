<?php namespace Sspoon\Backend\Controllers;

use Backend\Classes\Controller;
use BackendMenu;
use Input;
use October\Rain\Support\Facades\Flash;
use RainLab\Translate\Models\Locale;
use Sspoon\Backend\Models\Propos as ProposModel;
use Validator;
class Propos extends Controller
{
    public $implement = [];

    public function __construct()
    {
        parent::__construct();

        BackendMenu::setContext('Sspoon.Backend', 'main-menu-propos');
    }

    public function index()
    {
        $s = ProposModel::instance();
        $this->vars['data'] = $s;
    }

    public function onUpdate()
    {
        $input = Input::get('data');

        $rules = ['title' => 'required'];

        $validator = Validator::make($input, $rules);

        if (!$validator->fails()) {
            $s = ProposModel::instance();

            $s->fill($input)->save();
            Flash::success('Data successfully saved!');
        } else {
            Flash::error('Title is required.');
        }


        return \Redirect::to('/backend/sspoon/backend/propos');
    }


}
