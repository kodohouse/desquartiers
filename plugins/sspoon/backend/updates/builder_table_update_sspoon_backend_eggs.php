<?php namespace Sspoon\Backend\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableUpdateSspoonBackendEggs extends Migration
{
    public function up()
    {
        Schema::table('sspoon_backend_eggs', function($table)
        {
            $table->text('link')->nullable();
            $table->increments('id')->unsigned(false)->change();
        });
    }
    
    public function down()
    {
        Schema::table('sspoon_backend_eggs', function($table)
        {
            $table->dropColumn('link');
            $table->increments('id')->unsigned()->change();
        });
    }
}
