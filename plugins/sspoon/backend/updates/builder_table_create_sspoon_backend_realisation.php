<?php namespace Sspoon\Backend\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableCreateSspoonBackendRealisation extends Migration
{
    public function up()
    {
        Schema::create('sspoon_backend_realisation', function($table)
        {
            $table->engine = 'InnoDB';
            $table->increments('id');
            $table->string('title', 255)->nullable();
            $table->string('address', 255)->nullable();
            $table->text('description')->nullable();
            $table->double('square', 10, 0)->nullable();
            $table->boolean('is_on_sale')->nullable();
        });
    }
    
    public function down()
    {
        Schema::dropIfExists('sspoon_backend_realisation');
    }
}
