<?php namespace Sspoon\Backend\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;
use Sspoon\Backend\Models\Professions;

class builder_table_create_record_professions extends Migration
{
    public function up()
    {

        \Db::table('sspoon_backend_professions')->insert([
            ['id' => 1],

        ]);

    }
    
    public function down()
    {
        \Db::table('sspoon_backend_professions')->delete();

       }
}
