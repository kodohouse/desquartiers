<?php namespace Sspoon\Backend\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableUpdateSspoonBackendEggs3 extends Migration
{
    public function up()
    {
        Schema::table('sspoon_backend_eggs', function($table)
        {
            $table->renameColumn('lon', 'lng');
        });
    }
    
    public function down()
    {
        Schema::table('sspoon_backend_eggs', function($table)
        {
            $table->renameColumn('lng', 'lon');
        });
    }
}
