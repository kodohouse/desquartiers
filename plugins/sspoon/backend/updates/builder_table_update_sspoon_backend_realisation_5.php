<?php namespace Sspoon\Backend\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableUpdateSspoonBackendRealisation5 extends Migration
{
    public function up()
    {
        Schema::table('sspoon_backend_realisation', function($table)
        {
            $table->string('lat', 30)->nullable()->change();
            $table->string('lng', 30)->nullable()->change();
        });
    }
    
    public function down()
    {
        Schema::table('sspoon_backend_realisation', function($table)
        {
            $table->string('lat', 30)->nullable(false)->change();
            $table->string('lng', 30)->nullable(false)->change();
        });
    }
}
