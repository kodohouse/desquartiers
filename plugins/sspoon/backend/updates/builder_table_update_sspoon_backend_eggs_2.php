<?php namespace Sspoon\Backend\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableUpdateSspoonBackendEggs2 extends Migration
{
    public function up()
    {
        Schema::table('sspoon_backend_eggs', function($table)
        {
            $table->string('lat', 30)->nullable()->unsigned(false)->default(null)->change();
            $table->string('lon', 30)->nullable()->unsigned(false)->default(null)->change();
        });
    }
    
    public function down()
    {
        Schema::table('sspoon_backend_eggs', function($table)
        {
            $table->double('lat', 10, 0)->nullable()->unsigned(false)->default(null)->change();
            $table->double('lon', 10, 0)->nullable()->unsigned(false)->default(null)->change();
        });
    }
}
