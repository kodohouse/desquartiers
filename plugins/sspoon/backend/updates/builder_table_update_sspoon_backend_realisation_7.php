<?php namespace Sspoon\Backend\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableUpdateSspoonBackendRealisation7 extends Migration
{
    public function up()
    {
        Schema::table('sspoon_backend_realisation', function($table)
        {
            $table->string('price', 100)->nullable()->unsigned(false)->default(null)->change();
            $table->string('lat', 30)->change();
        });
    }
    
    public function down()
    {
        Schema::table('sspoon_backend_realisation', function($table)
        {
            $table->double('price', 10, 0)->nullable()->unsigned(false)->default(null)->change();
            $table->string('lat', 100)->change();
        });
    }
}
