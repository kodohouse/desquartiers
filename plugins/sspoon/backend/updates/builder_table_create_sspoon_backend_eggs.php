<?php namespace Sspoon\Backend\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableCreateSspoonBackendEggs extends Migration
{
    public function up()
    {
        Schema::create('sspoon_backend_eggs', function($table)
        {
            $table->engine = 'InnoDB';
            $table->increments('id');
            $table->string('title', 255)->nullable();
            $table->string('address', 255);
            $table->double('lat', 10, 0)->nullable();
            $table->double('lon', 10, 0)->nullable();
            $table->text('description')->nullable();
        });
    }
    
    public function down()
    {
        Schema::dropIfExists('sspoon_backend_eggs');
    }
}
