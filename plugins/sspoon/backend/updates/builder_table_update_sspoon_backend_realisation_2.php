<?php namespace Sspoon\Backend\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableUpdateSspoonBackendRealisation2 extends Migration
{
    public function up()
    {
        Schema::table('sspoon_backend_realisation', function($table)
        {
            $table->integer('sort_order')->default(0)->change();
        });
    }
    
    public function down()
    {
        Schema::table('sspoon_backend_realisation', function($table)
        {
            $table->integer('sort_order')->default(null)->change();
        });
    }
}
