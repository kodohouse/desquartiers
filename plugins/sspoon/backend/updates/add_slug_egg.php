<?php namespace Sspoon\Backend\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class add_slug_egg extends Migration
{
    public function up()
    {
        Schema::table('sspoon_backend_eggs', function($table)
        {
            $table->string('slug')->nullable();
        });
    }
    
    public function down()
    {
        Schema::table('sspoon_backend_eggs', function($table)
        {
            $table->dropColumn('slug');
        });
    }
}
