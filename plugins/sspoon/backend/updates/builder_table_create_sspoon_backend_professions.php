<?php namespace Sspoon\Backend\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableCreateSspoonBackendProfessions extends Migration
{
    public function up()
    {
        Schema::create('sspoon_backend_professions', function($table)
        {
            $table->engine = 'InnoDB';
            $table->increments('id');
            $table->text('title1')->nullable();
            $table->text('description1')->nullable();
            $table->text('title2')->nullable();
            $table->text('description2')->nullable();
            $table->text('title3')->nullable();
            $table->text('description4')->nullable();
        });
    }
    
    public function down()
    {
        Schema::dropIfExists('sspoon_backend_professions');
    }
}
