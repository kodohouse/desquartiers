<?php namespace Sspoon\Backend\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class add_city_and_postal_code_realisation extends Migration
{
    public function up()
    {
        Schema::table('sspoon_backend_realisation', function($table)
        {
            $table->string('city')->nullable();
            $table->string('postalcode')->nullable();
        });
    }
    
    public function down()
    {
        Schema::table('sspoon_backend_realisation', function($table)
        {
            $table->dropColumns('city', 'postalcode');
        });
    }
}
