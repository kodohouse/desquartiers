<?php namespace Sspoon\Backend\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableUpdateSspoonBackendRealisation4 extends Migration
{
    public function up()
    {
        Schema::table('sspoon_backend_realisation', function($table)
        {
            $table->string('lat', 30);
            $table->string('lng', 30);
        });
    }
    
    public function down()
    {
        Schema::table('sspoon_backend_realisation', function($table)
        {
            $table->dropColumn('lat');
            $table->dropColumn('lng');
        });
    }
}
