<?php namespace Sspoon\Backend\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableUpdateSspoonBackendRealisation extends Migration
{
    public function up()
    {
        Schema::table('sspoon_backend_realisation', function($table)
        {
            $table->integer('sort_order')->nullable();
        });
    }
    
    public function down()
    {
        Schema::table('sspoon_backend_realisation', function($table)
        {
            $table->dropColumn('sort_order');
        });
    }
}
