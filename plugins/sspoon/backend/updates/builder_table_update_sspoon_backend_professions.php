<?php namespace Sspoon\Backend\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableUpdateSspoonBackendProfessions extends Migration
{
    public function up()
    {
        Schema::table('sspoon_backend_professions', function($table)
        {
            $table->renameColumn('description4', 'description3');
        });
    }
    
    public function down()
    {
        Schema::table('sspoon_backend_professions', function($table)
        {
            $table->renameColumn('description3', 'description4');
        });
    }
}
