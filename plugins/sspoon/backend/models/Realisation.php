<?php namespace Sspoon\Backend\Models;

use Model;

/**
 * Model
 */
class Realisation extends Model
{
    use \October\Rain\Database\Traits\Validation;
    use \October\Rain\Database\Traits\Sortable;
    use \October\Rain\Database\Traits\Sluggable;

    /*
     * Disable timestamps by default.
     * Remove this line if timestamps are defined in the database table.
     */
    public $timestamps = false;


    /**
     * @var string The database table used by the model.
     */
    public $table = 'sspoon_backend_realisation';


    public $attachMany = [
        'images' => ['System\Models\File'],
    ];

    /**
     * @var array Validation rules
     */
    public $rules = [
        'slug' => 'unique:sspoon_backend_realisation',
        'title'=> 'required',
        'address'=> 'required',
    ];

    protected $slugs = ['slug' => 'title'];
}
