<?php

namespace Sspoon\Backend\Models;

use Model;
use Db;


class Propos extends Model
{
    public $implement = ['System.Behaviors.SettingsModel'];

    // A unique code
    public $settingsCode = 'propos_settings';

    // Reference to field configuration
    public $settingsFields = false;

    public $rules = [
        'title'=> 'required'
    ];

}
