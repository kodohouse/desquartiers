<?php

namespace Sspoon\Backend\Models;

use Model;
use Db;


class Contact extends Model
{
    public $implement = ['System.Behaviors.SettingsModel'];

    // A unique code
    public $settingsCode = 'contact_settings';

    // Reference to field configuration
    public $settingsFields = false;



}
