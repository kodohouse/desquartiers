<?php namespace Sspoon\Backend\Models;

use Model;

/**
 * Model
 */
class Professions extends Model
{
    use \October\Rain\Database\Traits\Validation;
    
    /*
     * Disable timestamps by default.
     * Remove this line if timestamps are defined in the database table.
     */
    public $timestamps = false;


    /**
     * @var string The database table used by the model.
     */
    public $table = 'sspoon_backend_professions';

    /**
     * @var array Validation rules
     */
    public $rules = [
        'title1'=> 'required',
        'title2'=> 'required',
        'title3'=> 'required'
    ];
    public $attachOne = [
        'logotype1_1' => ['System\Models\File'],
        'logotype1_2' => ['System\Models\File'],
        'logotype1_3' => ['System\Models\File'],
        'logotype2_1' => ['System\Models\File'],
        'logotype2_2' => ['System\Models\File'],
        'logotype2_3' => ['System\Models\File'],
        'logotype3_1' => ['System\Models\File'],
        'logotype3_2' => ['System\Models\File'],
        'logotype3_3' => ['System\Models\File'],
    ];
}
