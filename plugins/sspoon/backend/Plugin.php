<?php namespace Sspoon\Backend;

use System\Classes\PluginBase;

class Plugin extends PluginBase
{
    public function registerComponents()
    {
        return ['Sspoon\Backend\Components\Descriptions' => 'descriptions',
            'Sspoon\Backend\Components\Title' => 'title'
        ];
    }

    public function registerSettings()
    {
    }

    public function registerMarkupTags()
    {
        return [
            'functions' => [

                'getImageSizeAttributes' => function ($value) {

                    if (!empty($value->getDiskPath())) {

                        $filePath = storage_path('app/' . $value->getDiskPath());

                        if (is_file($filePath)) {

                            list($width, $height, $type, $attributes) = getimagesize($filePath);
                            return ['width' => $width, 'height' => $height];

                        }

                    }

                }
            ]
        ];
    }
}
