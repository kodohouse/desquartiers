'use strict';

var ASSETS_DIR = './themes/desquartiers/assets/';

var gulp         = require('gulp'),
    sass         = require('gulp-ruby-sass'),
    rename       = require('gulp-rename'),
    uglify       = require('gulp-uglify'),
    babelify     = require('babelify'),
    source       = require('vinyl-source-stream'),
    buffer       = require('vinyl-buffer'),
    browserify   = require('browserify'),
    sourcemaps   = require('gulp-sourcemaps'),
    autoprefixer = require('gulp-autoprefixer'),
    cleanCSS     = require('gulp-clean-css'),
    sassLint     = require('gulp-sass-lint');

/**
 * Default task - 'gulp' command
 */
gulp.task('default', ['serve']);

/**
 * Static Server + watching source files
 */
gulp.task('serve', ['sass-lint', 'sass-compile', 'es6'], function() {
    gulp.watch(ASSETS_DIR + 'sass/**/*', ['sass-lint', 'sass-compile']);
    gulp.watch(ASSETS_DIR + 'js/src/*', ['es6']);
});

/**
 * Lint sass
 */
 gulp.task('sass-lint', function () {
   return gulp.src([ASSETS_DIR + 'sass/**/*.scss', '!' + ASSETS_DIR + 'sass/**/*-nolint.scss'])
     .pipe(sassLint({
       configFile: '.sass-lint.yml'
     }))
     .pipe(sassLint.format())
     .pipe(sassLint.failOnError())
 });

/**
 * Compile with gulp-ruby-sass + source maps
 */
gulp.task('sass-compile', function () {
    return sass(ASSETS_DIR + 'sass/main.scss', {sourcemap: true})
        .on('error', function (err) {
            console.error('Error!', err.message);
        })
        .pipe(sourcemaps.init())
        .pipe(autoprefixer({
            browsers: ['last 10 versions'],
            cascade: false
        }))
        .pipe(sourcemaps.write('./', {
            includeContent: false,
            sourceRoot: ASSETS_DIR + 'sass'
        }))
        .pipe(gulp.dest(ASSETS_DIR + 'css'))
});

/**
 * Babelify ES6
 */
gulp.task('es6', () => {
    return browserify(ASSETS_DIR + 'js/src/main.js')
        .transform('babelify', {loose : 'all'})
        .bundle()
        .pipe(source('main.min.js'))
        .pipe(buffer())
        // .pipe(uglify())
        .pipe(gulp.dest(ASSETS_DIR + 'js/dist'));
});


gulp.task('compress', function () {
    gulp.src(ASSETS_DIR + 'js/main.js')
        .pipe(uglify())
        .pipe(rename({
            suffix: '.min'
        }))
        .pipe(gulp.dest(ASSETS_DIR + 'js'))

    gulp.src(ASSETS_DIR + 'css/main.css')
        .pipe(cleanCSS({compatibility: 'ie8'}))
        .pipe(rename({
            suffix: '.min'
        }))
        .pipe(gulp.dest(ASSETS_DIR + 'css'));
});
